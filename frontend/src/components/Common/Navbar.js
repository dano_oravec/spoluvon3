import React from 'react';
import { withRouter } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { withStyles } from "@material-ui/core/styles";


const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  button: {
    textTransform: "none",
  },
  title: {
    flexGrow: 1,
  },
});


class Navbar extends React.Component {
  constructor(props) {
    super(props);
    this.logOut = this.logOut.bind(this);
    this.home = this.home.bind(this);
    this.register = this.register.bind(this);
  }

  logOut() {
    localStorage.removeItem('authtoken');
    this.props.userLoggedOut();
    this.props.history.push('/');
  }

  home() {
    this.props.history.push('/');
  }

  register() {
    this.props.history.push('/register');
  }

  render() {
    const { classes } = this.props;
    if(!this.props.loggedIn)
      return <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title} onClick={this.home}>
            SpoluVon
          </Typography>
          <Button color="inherit" className={classes.button} onClick={this.home}>Prihlásiť</Button>
          <Button color="inherit" className={classes.button} onClick={this.register}>Registrovať</Button>
        </Toolbar>
      </AppBar>
    </div>;

    return <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title} onClick={this.home}>
            SpoluVon
          </Typography>
          <Button color="inherit" className={classes.button} onClick={this.logOut}>Odhlásiť</Button>
        </Toolbar>
      </AppBar>
    </div>
  }
}

export default withStyles(styles, {withTheme: true})(withRouter(Navbar));