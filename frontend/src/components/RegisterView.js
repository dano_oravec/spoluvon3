import React from 'react';
import { withRouter } from 'react-router-dom';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import Alert from '@material-ui/lab/Alert';
import Grid from '@material-ui/core/Grid';

import FormError from './Errors';


class RegisterSuccessAlert extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    if(this.props.display)
      return <Alert variant="filled" severity="success">
        Registrácia takmer dokončená. Potvrďte prosím svoj email kliknutím na odkaz,
        ktorý sme vám zaslali na <strong>{this.props.email}</strong>.
      </Alert>;
    return <div></div>;
  }
}


class RegisterView extends React.Component {
  constructor(props) {
    super(props);
    if(props.loggedIn)
      this.props.history.push("/dashboard");
    this.state = {
      username: '',
      email: '',
      password: '',
      password2: '',
      usernameError: '',
      emailError: '',
      passwordError: '',
      generalError: '',
      registrationFinished: false
    };
    this.setAttribute = this.setAttribute.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  setAttribute(e) {
    this.setState({[event.target.name]: e.target.value});
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState( {
      usernameError: '',
      emailError: '',
      passwordError: ''
    });
    const { username, email, password, password2 } = this.state;
    const url = 'http://127.0.0.1:8000/api/account/register/';
    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        username: username,
        email: email,
        password: password,
        password2: password2
      })
    }).then((response) => {
        if(response.ok)
          return response.text();
        throw new Error('Registration failed');
    }).then(results => {
        let data = JSON.parse(results);
        if(data['token'] === undefined) {
          if(data['username'] !== undefined)
            this.setState({usernameError: data['username']});
          if(data['email'] !== undefined)
            this.setState({emailError: data['email']});
          if(data['password'] !== undefined)
            this.setState({passwordError: data['password']});
        }
        else {
          this.setState({registrationFinished: true});
        }
      }).catch((err) => {
          this.setState({generalError: ['Nepodarilo sa zaregistrovať. Skúste prosím zopakovať registráciu.']});
    });
  }

  render() {
    return <div>
      <RegisterSuccessAlert display={this.state.registrationFinished} email={this.state.email} />
      <Grid container spacing={0} direction="column" alignItems="center" justify="center" style={{ minHeight: '100vh' }}>
      <Box maxWidth="300px" margin="auto">
        <Paper elevation={3}>
          <Box p={2} m="auto">
            <form method="post" onSubmit={this.handleSubmit}>
              <Box m={2}>
                <FormError text={this.state.usernameError} />
                <TextField name="username" label="Používateľské meno" onChange={e => this.setAttribute(e)} /><br/>
              </Box>
              <Box m={2}>
                <FormError text={this.state.emailError} />
                <TextField name="email" label="Email" onChange={e => this.setAttribute(e)} /><br/>
              </Box>
              <Box m={2}>
                <FormError text={this.state.passwordError} />
                <TextField name="password" label="Heslo" type="password" onChange={e => this.setAttribute(e)} /><br/>
              </Box>
              <Box m={2}>
                <TextField name="password2" label="Potvrdenie hesla" type="password" onChange={e => this.setAttribute(e)} /><br/>
              </Box>
              <Box m={2}>
                <FormError text={this.state.generalError} />
                <Button type="submit" color="secondary" variant="contained" style={{width: "100%"}}>
                  <span style={{color: '#ffffff'}}>Registrovať</span>
                </Button>
              </Box>
            </form>
          </Box>
        </Paper>
      </Box>
      </Grid>
    </div>
  }
}

export default withRouter(RegisterView);