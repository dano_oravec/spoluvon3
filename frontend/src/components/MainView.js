import React from 'react';


class MainView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loaded: false,
      data: [],
      page: 1
    };
    this.getData = this.getData.bind(this);
    this.getData();
  }

  getData() {
    const params = {
      page: this.state.page
    };
    const host = 'http://127.0.0.1:8000';
    const path = '/api/activities/dashboard/?';
    let url = new URL(host + path) + new URLSearchParams(params).toString();
    fetch(url, {
      method: 'GET',
      headers: {
        'Authorization': 'Token ' + localStorage.getItem('authtoken')
      }
    })
        .then(results => results.json())
        .then((results) => {
          this.setState({
            loaded: true,
            data: results
          })
        });
  }

  render() {
    if(!this.state.loaded)
      return 'Loading...';
    return <h1>MainView</h1>
  }
}

export default MainView;