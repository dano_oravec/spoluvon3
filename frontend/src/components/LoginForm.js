import React from 'react';
import { withRouter } from 'react-router-dom';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';

import Errors from './Errors';


class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      loginErrors: [],
    };
    this.setUsername = this.setUsername.bind(this);
    this.setPassword = this.setPassword.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleRegister = this.handleRegister.bind(this);
  }

  setUsername(e) {
    this.setState({username: e.target.value});
  }

  setPassword(e) {
    this.setState({password: e.target.value});
  }

  handleSubmit(event) {
    event.preventDefault();
    const { username, password } = this.state;
    const url = 'http://127.0.0.1:8000/api/account/login/';
    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({username: username, password: password})
    }).then((response) => {
        if(response.ok)
          return response.text();
        throw new Error('Login failed');
    }).then(results => {
        let data = JSON.parse(results);
        localStorage.setItem('authtoken', data.token);
        this.props.userLoggedIn();
        this.props.history.push("/dashboard");
      }).catch((err) => {
          this.setState({loginErrors: ['Nepodarilo sa prihlásiť']});
    });
  }

  handleRegister() {
    this.props.history.push("/register");
  }

  render() {
    return <Box maxWidth="300px" margin="auto">
      <Paper elevation={3}>
        <Box p={2} m="auto">
          <form method="post" onSubmit={this.handleSubmit}>
            <Errors errors={this.state.loginErrors} />
            <Box m={2}>
              <TextField name="username" label="Email" onChange={e => this.setUsername(e)} /><br/>
            </Box>
            <Box m={2}>
              <TextField name="password" label="Heslo" type="password" onChange={e => this.setPassword(e)} /><br/>
            </Box>
            <Box m={2}>
              <Button type="submit" color="secondary" variant="contained" style={{width: "100%"}}><span style={{color: '#ffffff'}}>Prihlásiť</span></Button>
            </Box>
          </form>
          <Divider light />
          <Box m={2}>
            <p className="footnote">Nemáte ešte účet?</p>
          </Box>
          <Box m={2}>
            <Button onClick={this.handleRegister} color="primary" variant="contained" style={{width: "100%"}}>
              <span style={{color: '#ffffff'}}>Registrovať</span>
            </Button>
          </Box>
        </Box>
      </Paper>
    </Box>
  }
}

export default withRouter(LoginForm);