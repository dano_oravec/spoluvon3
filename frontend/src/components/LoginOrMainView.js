import React from "react";
import { withRouter } from "react-router-dom";

import HomeView from "./HomeView";

class LoginOrMainView extends React.Component {
  constructor(props) {
    super(props);
    if(this.props.loggedIn)
      this.props.history.push('/dashboard');
  }

  render() {
    return <HomeView userLoggedIn={this.props.userLoggedIn}/>;
  }
}

export default withRouter(LoginOrMainView);
