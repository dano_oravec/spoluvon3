import React from "react";
import { withRouter } from 'react-router-dom';

import MainView from "./MainView";

class MainViewOrLogin extends React.Component {
  constructor(props) {
    super(props);
    if(!this.props.loggedIn)
      this.props.history.push('/');
  }

  render() {
    return <MainView/>;
  }
}

export default(withRouter(MainViewOrLogin));