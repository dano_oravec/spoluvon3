import React from "react";


export default class FormError extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    if(!this.props.text)
      return <div></div>;
    return <ul style={{color: '#ff0000'}}>
      <li>{this.props.text}</li>
    </ul>
  }
}


// export default class Errors extends React.Component {
//   constructor(props) {
//     super(props);
//   }
//
//   render() {
//     let errorItems = this.props.errors.map((err) => <li key={err}>{err}</li>);
//     return <ul style={{color: '#ff0000'}}>
//       {errorItems}
//     </ul>
//   }
// }
