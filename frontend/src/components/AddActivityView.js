import React from 'react';
import { withRouter } from 'react-router-dom';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import { DateTimePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from "@date-io/date-fns";

import FormError from './Errors';
import LocationAutocomplete from './Common/LocationAutocomplete';


class AddActivityView extends React.Component {
  constructor(props) {
    super(props);
    if(!this.props.loggedIn)
      this.props.history.push('/');
    this.state = {
      loaded: false,
      tags: null, // All tags
      name: null,
      selectedTags: [], // Only selected
      dateTimeBegin: new Date(),
      dateTimeEnd: new Date(),
      minPeople: '',
      maxPeople: '',
      description: null,
      location: null,
      additionalInfo: null,
      errors: {
        name: null,
        selectedTags: null,
        dateTimeBegin: null,
        dateTimeEnd: null,
        minPeople: null,
        maxPeople: null,
        description: null,
        location: null,
        additionalInfo: null
      }
    };
    this.loadData = this.loadData.bind(this);
    this.setAttribute = this.setAttribute.bind(this);
    this.handleDateTimeBeginChange = this.handleDateTimeBeginChange.bind(this);
    this.handleDateTimeEndChange = this.handleDateTimeEndChange.bind(this);
    this.handleTagsChange = this.handleTagsChange.bind(this);
    this.handleActivitySubmit = this.handleActivitySubmit.bind(this);
    this.handleLocationChange = this.handleLocationChange.bind(this);
    this.loadData();
  }

  loadData() {
    const params = {};
    const host = 'http://127.0.0.1:8000';
    const path = '/api/activities/tag_list/?';
    let url = new URL(host + path) + new URLSearchParams(params).toString();
    fetch(url, {
      method: 'GET',
      headers: {
        'Authorization': 'Token ' + localStorage.getItem('authtoken')
      },
    })
        .then(results => results.json())
        .then((results) => {
          this.setState({
            loaded: true,
            tags: results['tags']
          })
        })
        .catch((err) => {
          console.log(err);
        });
  };

  setAttribute(e) {
    this.setState({[e.target.name]: e.target.value});
  }

  setIntAttribute(e, mn) {
    let value = e.target.value;
    let onlyDigits = value.replace(/[^0-9]/g, '');
    if(value.length === 0)
      value = '';
    else if(value.length !== onlyDigits.length)
      value = mn ? this.state.minPeople : this.state.maxPeople;
    else
      value = parseInt(value);
    if(mn)
      this.setState({minPeople: value});
    else
      this.setState({maxPeople: value});
  }

  handleDateTimeBeginChange(dt) {
    this.setState({dateTimeBegin: dt});
  }

  handleDateTimeEndChange(dt) {
    this.setState({dateTimeEnd: dt});
  }

  handleTagsChange(e, value) {
    this.setState({selectedTags: value})
  }

  handleLocationChange(value) {
    this.setState({location: value});
  }

  handleActivitySubmit() {
    const params = {};
    const host = 'http://127.0.0.1:8000';
    const path = '/api/activities/add_activity/?';
    let url = new URL(host + path) + new URLSearchParams(params).toString();
    let st = this.state;
    const data = {
      name: st.name,
      selectedTags: st.selectedTags,
      dateTimeBegin: st.dateTimeBegin,
      dateTimeEnd: st.dateTimeEnd,
      description: st.description,
      location: st.location,
      additionalInfo: st.additionalInfo,
      minPeople: st.minPeople,
      maxPeople: st.maxPeople
    };
    fetch(url, {
      method: 'POST',
      headers: {
        'Authorization': 'Token ' + localStorage.getItem('authtoken'),
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data)
    })
        .then(results => results.json())
        .then((results) => {
          this.setState({errors: results['errors']});
        })
        .catch((err) => {
          console.log('error');
          console.log(err);
        });
  }

  render() {
    if(!this.state.loaded)
      return <p>Loading...</p>;
    return <div>
      <Box maxWidth="300px" margin="auto">
        <Paper elevation={3}>
          <Box p={2} m="auto">
            <form>
              <Box m={2}>
                <FormError text={this.state.errors['name']}/>
                <TextField name="name" label={'Názov aktivity'} style={{width: "100%"}} onChange={this.setAttribute} />
              </Box>
              <Box m={2}>
                <FormError text={this.state.errors['selectedTags']}/>
                <Autocomplete
                  multiple
                  name="tags"
                  style={{width: "100%"}}
                  options={this.state.tags}
                  getOptionLabel={(option) => option.name}
                  renderInput={(params) => <TextField {...params} label="Tagy" />}
                  onChange={this.handleTagsChange}
                />
              </Box>
              <Box m={2}>
                <FormError text={this.state.errors['dateTimeBegin']}/>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <DateTimePicker
                      name='dateTimeBegin'
                      value={this.state.dateTimeBegin}
                      onChange={this.handleDateTimeBeginChange}
                      ampm={false}
                      disablePast
                      label={'Dátum a čas začiatku'}
                  />
                </MuiPickersUtilsProvider>
              </Box>
              <Box m={2}>
                <FormError text={this.state.errors['dateTimeEnd']}/>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <DateTimePicker
                      name='dateTimeEnd'
                      value={this.state.dateTimeEnd}
                      onChange={this.handleDateTimeEndChange}
                      ampm={false}
                      disablePast
                      label={'Dátum a čas konca'}
                  />
                </MuiPickersUtilsProvider>
              </Box>
              <Box m={2}>
                <FormError text={this.state.errors['description']}/>
                <TextField name="description" multiline rows={1} rowsMax={6} label="Popis aktivity" style={{width: "100%"}} onChange={this.setAttribute} />
              </Box>
              <Box m={2}>
                <FormError text={this.state.errors['location']}/>
                <LocationAutocomplete name="location" autocompleteLabel="Miesto" handleLocationChange={this.handleLocationChange}/>
              </Box>
              <Box m={2}>
                <FormError text={this.state.errors['minPeople']}/>
                <TextField
                    name='minPeople'
                    label='Minimálny počet hostí'
                    value={this.state.minPeople}
                    style={{width: "100%"}}
                    onChange={(e) => {this.setIntAttribute(e, true);}}
                />
              </Box>
              <Box m={2}>
                <FormError text={this.state.errors['maxPeople']}/>
                <TextField
                    name='maxPeople'
                    label='Maximálny počet hostí'
                    value={this.state.maxPeople}
                    style={{width: "100%"}}
                    onChange={(e) => {this.setIntAttribute(e, false);}}
                />
              </Box>
              <Box m={2}>
                <FormError text={this.state.errors['additionalInfo']}/>
                <TextField name="additionalInfo" label="Doplňujúce informácie" onChange={this.setAttribute} />
              </Box>
              <Box m={2}>
                <Button onClick={this.handleActivitySubmit} color="secondary" variant="contained" style={{width: "100%"}}>
                  <span style={{color: "#ffffff"}}>Pridať</span>
                </Button>
              </Box>
            </form>
          </Box>
        </Paper>
      </Box>
    </div>;
  }
}

export default withRouter(AddActivityView);