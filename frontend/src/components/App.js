import React from 'react';
import { render } from 'react-dom';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';
import { withRouter } from 'react-router-dom';

import { createMuiTheme } from "@material-ui/core";
import { ThemeProvider } from "@material-ui/styles";
import { deepOrange, lightBlue } from "@material-ui/core/colors"

import Navbar from "./Common/Navbar";
import MainViewOrLogin from "./MainViewOrLogin";
import LoginOrMainView from "./LoginOrMainView";
import RegisterView from "./RegisterView";
import AddActivityView from "./AddActivityView";


const theme = createMuiTheme({
  palette: {
    primary: deepOrange,
    secondary: lightBlue,
  }
});


class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loaded: false,
      placeholder: 'Loading...',
      loggedIn: (localStorage.getItem('authtoken') != null)
    };
    this.userLoggedIn = this.userLoggedIn.bind(this);
    this.userLoggedOut = this.userLoggedOut.bind(this);
  }

  userLoggedIn() {
    this.setState({loggedIn: true});
  }

  userLoggedOut() {
    this.setState({loggedIn: false});
  }

  render() {
    return <Router>
      <ThemeProvider theme={theme}>
        <Navbar loggedIn={this.state.loggedIn} userLoggedOut={this.userLoggedOut} />
        <div>
          <Switch>
            <Route exact path="/dashboard">
              <MainViewOrLogin loggedIn={this.state.loggedIn} />
            </Route>
            <Route exact path="/">
              <LoginOrMainView loggedIn={this.state.loggedIn} userLoggedIn={this.userLoggedIn} />
            </Route>
            <Route exact path="/register">
              <RegisterView loggedIn={this.state.loggedIn} />
            </Route>
            <Route exact path="/activities/add">
              <AddActivityView loggedIn={this.state.loggedIn} />
            </Route>
          </Switch>
        </div>
      </ThemeProvider>
    </Router>
  }
}

export default withRouter(App);

const container = document.getElementById("app");
render(<App />, container);