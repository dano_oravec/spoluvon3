import React from 'react';
import { withRouter } from 'react-router-dom';

import Grid from '@material-ui/core/Grid';

import LoginForm from './LoginForm';

class HomeView extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <Grid
      container
      spacing={0}
      direction="column"
      alignItems="center"
      justify="center"
      style={{ minHeight: '100vh' }}
    >
      <Grid item xs={12} sm={9} md={6} lg={3}>
        <LoginForm userLoggedIn={this.props.userLoggedIn}/>
      </Grid>
    </Grid>;
  }
}

export default withRouter(HomeView);