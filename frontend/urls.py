from django.urls import path, re_path
from . import views


urlpatterns = [
    path(r'', views.index),  # This has to be last
    re_path(r'^.*$', views.index)  # This has to be last
]
