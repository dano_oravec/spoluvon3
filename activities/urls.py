from django.urls import path
from activities import views


urlpatterns = [
    path(r'dashboard/', views.DashboardView.as_view(), name='dashboard_view'),
    path(r'tag_list/', views.TagList.as_view(), name='tag_list'),
    path(r'add_activity/', views.AddActivity.as_view(), name='add_activity')
]
