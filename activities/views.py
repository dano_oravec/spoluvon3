from rest_framework.views import APIView, Response
from rest_framework.permissions import IsAuthenticated
import datetime
import requests
import json

from .models import Tag, Activity, Location


class DashboardView(APIView):
    permission_classes = [IsAuthenticated]
    PER_PAGE = 10

    def get(self, request, format=None):
        page = request.GET.get('page')
        content = {
            'status': 'permitted'
        }
        return Response(content)


class TagList(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        tags = Tag.objects.all()
        out_list = []
        for t in tags:
            out_list.append({'name': t.name})
        content = {'tags': out_list}
        return Response(content)


class AddActivity(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        # request.data keys = [
        #   name, selectedTags, dateTimeBegin, dateTimeEnd, description, location, additionalInfo, minPeople, maxPeople
        # ]
        errors = {}
        data = request.data

        place_id = data['location']['place_id']
        try:
            location = Location.objects.get(place_id=place_id)
        except Location.DoesNotExist:
            r = requests.get(
                f'https://maps.googleapis.com/maps/api/geocode/json?place_id={place_id}&key=AIzaSyD6Wk1--sBQcYVyPabdhCMEK3Z_VaL-eOc'
            )
            try:
                location_data = json.loads(r.text)['results'][0]
            except IndexError:
                errors['location'] = 'Neznáme miesto'
                return Response({'status': 'fail', 'errors': errors})
            region, district = '', ''
            for comp in location_data['address_components']:
                if 'administrative_area_level_1' in comp['types']:
                    region = comp['long_name']
                elif 'administrative_area_level_2' in comp['types']:
                    district = comp['long_name']
            location = Location(
                place_id=place_id,
                place_name_main=data['location']['structured_formatting']['main_text'],
                place_name_secondary=data['location']['structured_formatting']['secondary_text'],
                lng=location_data['geometry']['location']['lng'],
                lat=location_data['geometry']['location']['lat'],
                region=region,
                district=district,
                description=data['location']['description']
            )
            location.save()

        activity = Activity(
            organizer=request.user,
            name=data['name'],
            description=data['description'],
            additional_info=data['additionalInfo'],
            datetime=data['dateTimeBegin'],
            location=location
        )
        begin = self.parse_dt(data['dateTimeBegin'])
        end = self.parse_dt(data['dateTimeEnd'])
        print(f'begin: {begin}')
        print(f'end: {end}')
        print(f'delta: {end - begin}')
        if begin > end:
            errors['dateTimeBegin'] = 'Aktivita začína neskôr, ako končí'
            return Response({'status': 'fail', 'errors': errors})
        activity.duration = end - begin
        activity.save()
        for tag in data['selectedTags']:
            activity.tags.add(Tag.objects.get(name=tag['name']))
        mn_people, mx_people = data['minPeople'], data['maxPeople']
        if isinstance(mn_people, str):
            mn_people = 0
        if isinstance(mx_people, str):
            mx_people = 1000000
        if mx_people > 1000000:
            errors['maxPeople'] = 'Maximálny možný počet ľudí je 1000000'
        if mn_people > mx_people:
            errors['minPeople'] = 'Minimálny počet ľudí môže byť nanajvýš taký, ako maximálny počet ľudí'
        if len(errors):
            return Response({'status': 'fail', 'errors': errors})
        activity.min_people = mn_people
        activity.max_people = mx_people
        activity.save()

        return Response({'status': 'success', 'errors': {}})

    @staticmethod
    def parse_dt(dt):
        year = int(dt[:4])
        month = int(dt[5:7])
        day = int(dt[8:10])
        hour = int(dt[11:13])
        minute = int(dt[14:16])
        return datetime.datetime(year=year, month=month, day=day, hour=hour, minute=minute)
