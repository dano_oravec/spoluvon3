from django.db import models
from django.db.migrations.serializer import BaseSerializer
from django.db.migrations.writer import MigrationWriter
from account.models import Account


class Tag(models.Model):
    name = models.CharField(max_length=60, null=False, unique=True, verbose_name='name')
    description = models.CharField(max_length=200, null=True, unique=False, verbose_name='description')

    def __str__(self):
        return self.name


class Location(models.Model):
    place_id = models.CharField(max_length=200, verbose_name='place id', default='', unique=True)
    place_name_main = models.CharField(max_length=200, verbose_name='main name', default='')
    place_name_secondary = models.CharField(max_length=300, verbose_name='secondary name', default='')
    lng = models.FloatField(verbose_name='longitude')
    lat = models.FloatField(verbose_name='latitude')
    region = models.CharField(max_length=100, default='')  # Kraj
    district = models.CharField(max_length=100, default='')  # Okres
    description = models.CharField(max_length=300, verbose_name='description', default='', unique=True)

    def __str__(self):
        return self.place_name_main


class LocationSerializer(BaseSerializer):
    def serialize(self):
        return self.value.place_id, {'from activities.models import Location'}


MigrationWriter.register_serializer(Location, LocationSerializer)


class Activity(models.Model):
    organizer = models.ForeignKey(Account, null=True, on_delete=models.CASCADE, verbose_name='organizer')
    name = models.CharField(max_length=60, null=False, unique=False, verbose_name='name')
    tags = models.ManyToManyField(Tag, verbose_name='tags')
    description = models.CharField(max_length=600, null=False, unique=False, verbose_name='description')
    location = models.ForeignKey(Location, on_delete=models.CASCADE, default=Location(place_id='0', place_name_main='', place_name_secondary='', lng=0, lat=0, description=''))
    min_people = models.IntegerField(verbose_name='min people', null=True)
    max_people = models.IntegerField(verbose_name='max people', null=True)
    additional_info = models.CharField(max_length=300, null=True, unique=False, verbose_name='additional info')
    datetime = models.DateTimeField(verbose_name='datetime', null=False)
    duration = models.DurationField(verbose_name='duration', null=False)

    def __str__(self):
        return f'{self.name}'
