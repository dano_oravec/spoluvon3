from django.contrib import admin
from .models import Tag, Activity, Location


admin.site.register(Tag)
admin.site.register(Activity)
admin.site.register(Location)
