from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/home/', include('home.urls')),
    path('api/account/', include('account.api.urls', 'account_api')),
    path('api/activities/', include('activities.urls')),
    path('', include('frontend.urls'))
]
