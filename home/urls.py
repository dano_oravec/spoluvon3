from django.urls import path
from . import views


urlpatterns = [
    path(r'', views.IndexAPIView.as_view(), name='index_api_view')
]
